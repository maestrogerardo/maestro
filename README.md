# maestro

Website of _Maestro Gerardo_.

## details

Actually, it's just one of several websites I'm working on.
Well, this particular one might be the first one I've ever
created on the basis of "shell scripts"!

Yeah, I know, there are lots of good reasons _not_ to do so
... _but where is the fun?!_ ;-)
