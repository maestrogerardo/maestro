#!/bin/bash
#
# This is a library -- meant to source, not to call!

# namespace gad

# config check
gad_CONFIG_SOURCED=

# global defaults
gad_URL="http://maestrogerardo.com/"
gad_LANG="en"
gad_TITLE="Maestro Gerardo"
gad_DESCR="Maestro Gerardo Stuff"
gad_AUTHOR="maestrogerardo (Gerhard A. Dittes)"
gad_CSS="base.css?v=0.4"
gad_ICON="g_32x32.png"
gad_ICON_SIZE="32x32"
gad_PATH_PREFIX="./"
