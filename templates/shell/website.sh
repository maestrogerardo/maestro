#!/bin/bash
#
# This is a library -- meant to source, not to call!

# namespace gad

# source config only once
[ -z "${gad_CONFIG_SOURCED}" ] && . "../../config.bash" && gad_CONFIG_SOURCED=1 || true


gad_printHttpHeader()
{
	printf "Content-Type: text/html\r\n"
	printf "Cache-Control: max-age=300\r\n\r\n"
}

gad_printHeader()
{
	cat <<EOF
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="${gad_LANG}">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="${gad_DESC}" />
		<meta name="author" content="${gad_AUTHOR}" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" type="text/css" href="${gad_PATH_PREFIX}style/${gad_CSS}" />
		<link rel="icon" sizes="${gad_ICON_SIZE}" href="${gad_PATH_PREFIX}icons/${gad_ICON}" />
		<title>${gad_TITLE}</title>
	</head>
	<body>
	<!-- HEADER END -->

EOF
}

gad_printFooter()
{
	cat <<EOF
	<!-- FOOTER START -->
	</body>
</html>
EOF
}

gad_printHttpRedirect()
{
	# printf "HTTP/1.1 302 Found\r\n"
	printf "Location: ${gad_URL}\r\n\r\n"
}

gad_processLang()
{
	local lang="$(echo ${QUERY_STRING} | cut -d'=' -f2)"

	if [ -n "${lang}" ]; then
		case "${lang}" in
			"es")
				gad_LANG="${lang}"
				# gad_TITLE...
				gad_DESCR="Trastos de Maestro Gerardo"
				# gad_AUTHOR
				# gad_CSS...
				;;
			"de")
				gad_LANG="${lang}"
				# gad_TITLE...
				gad_DESCR="Maestro Gerardo Kram"
				# gad_AUTHOR...
				# gad_CSS...
				;;
		esac
	fi
}

# arg 1: target script name
gad_printLangOption()
{
	local target="${1}"

	local classEn="active"
	local classEs="inactive"
	local classDe="inactive"

	local imgAltEn="English"
	local imgTitEn="Choose English language"
	local imgAltEs="Spanish"
	local imgTitEs="Choose Spanish language"
	local imgAltDe="German"
	local imgTitDe="Choose German language"

	case "${gad_LANG}" in
		"es")
			classEn="inactive"
			classEs="active"
			classDe="inactive"

			imgAltEn="Ingl&eacute;s"
			imgTitEn="Elige lengua inglesa"
			imgAltEs="Espa&ntilde;ol"
			imgTitEs="Elige lengua espa&ntilde;ola"
			imgAltDe="Alem&aacute;n"
			imgTitDe="Elige lengua alemana"
			;;
		"de")
			classEn="inactive"
			classEs="inactive"
			classDe="active"

			imgAltEn="Englisch"
			imgTitEn="W&auml;hle englische Sprache"
			imgAltEs="Spanisch"
			imgTitEs="W&auml;hle spanische Sprache"
			imgAltDe="Deutsch"
			imgTitDe="W&auml;hle deutsche Sprache"
			;;
	esac

	cat <<EOF
		<div id="language">
			<p>
				<a href="${target}?lang=en"><img class="${classEn}" alt="${imgAltEn}" title="${imgTitEn}" src="${gad_PATH_PREFIX}pics/flag_en.svg" /></a> |
				<a href="${target}?lang=es"><img class="${classEs}" alt="${imgAltEs}" title="${imgTitEs}" src="${gad_PATH_PREFIX}pics/flag_es.svg" /></a> |
				<a href="${target}?lang=de"><img class="${classDe}" alt="${imgAltDe}" title="${imgTitDe}" src="${gad_PATH_PREFIX}pics/flag_de.svg" /></a>
			</p>
		</div>
EOF
}
